#include <iostream>
#include <fstream>
#include <stdio.h>
#include <cstdlib>
#include <vector>
#include <list>
#include <map>
#include <string>
#include <set>
#include <cmath>
#include <algorithm>
#include <cilk/cilk.h>
#include <cilk/reducer_list.h>
#include <cilk/reducer_min.h>
#include <cilk/reducer_max.h>
#include <cilk/holder.h>

#include "graph_utils/graphUtils.h"
#include "graph_utils/graphIO.h"

#ifndef ENGINE_H
#define ENGINE_H

typedef struct engine_vertex {
  int partitionPoint;
  int order_degree;
  int counter;
  int partitionPoint2;
} engine_vertex;

template<typename VertexType, typename EdgeType>
class Engine {
  public:
    sparseRowMajor<int, int> sparse_rep;
    int* order;
    int* iorder;
    engine_vertex* vertex_data;
    int* partitionPoint;
    int* counters;
    int* order_degree;
    int* scheduled_bits;
    int* recorded_set_bits;
    int num_recorded_sets;
    std::vector<int>* recorded_sets;
    int scheduled_round;
    int* histogram;
    std::vector<int>* worklists;
    void (*update_function)(int);
    Engine();
    void run(int* schedule, int* iorder, void (*_update_function)(int));
    void run_worklist(int* schedule, int* worklist, int worklistsize, void (*_update_function)(int));
    void asyncUpdate(int v, int depth);
    void record_set(int v, int depth);
    void loadGraph(std::string inputname);
    void schedule(int v);
    inline bool scheduled(int v);
    inline bool in_recorded_set(int v);
    int get_worklist_size();
    int* get_worklist();
};

#endif
