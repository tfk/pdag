CC=icc
CFLAGS= -g -O3
CILK=icc
CILKFLAGS= -Wall -g -DCILKP -O3
LDFLAGS= -L$(CURDIR)
#AR=ar

DEPS= engine.cpp engine.h timeUtils.h Makefile

all: color bp iterative

color : apps/color.cpp $(DEPS)
	$(CILK) $(CILKFLAGS) apps/$@.cpp $(LDFLAGS) -o $@

bp : apps/bp.cpp $(DEPS)
	$(CILK) $(CILKFLAGS) apps/$@.cpp $(LDFLAGS) -o $@

iterative : apps/iterative.cpp $(DEPS)
	$(CILK) $(CILKFLAGS) apps/$@.cpp $(LDFLAGS) -o $@

clean :
	rm -f color bp iterative *~

run :
	touch V$(V)_D$(D).graph
	rm V$(V)_D$(D).graph
	python graph_gen.py $(V) $(D) >> V$(V)_D$(D).graph
	./main V$(V)_D$(D).graph
	rm V$(V)_D$(D).graph
