#include "engine.h"

static int coarsen_factor = 20000;
static int atomic_operations = 0;

inline int tfk_decrement(int u, engine_vertex* vertex_data) {
//  __sync_add_and_fetch(&atomic_operations, 1);
//  return __sync_sub_and_fetch(&counters[u], 1);
  return __sync_sub_and_fetch(&vertex_data[u].counter, 1);
}

//template<typename VertexType, typename EdgeType>
//Engine<VertexType, EdgeType>::Engine< VertexType,  EdgeType>() {
//}

template<typename VertexType, typename EdgeType>
Engine<VertexType, EdgeType>::Engine() {
  worklists = (std::vector<int>*) malloc(sizeof(std::vector<int>)*__cilkrts_get_total_workers());
  for (int i = 0; i < __cilkrts_get_total_workers(); i++) {
    worklists[i] = std::vector<int>();
  }
  histogram = (int*) calloc(100000,sizeof(int));
  recorded_sets = (std::vector<int>*) malloc(sizeof(std::vector<int>)*__cilkrts_get_total_workers());
  num_recorded_sets = 1;
  for (int i = 0; i < __cilkrts_get_total_workers(); i++) {
    recorded_sets[i] = std::vector<int>();
  }
}

template<typename VertexType, typename EdgeType>
int Engine<VertexType, EdgeType>::get_worklist_size() {
  int size = 0;
  for (int i = 0; i < __cilkrts_get_total_workers(); i++) {
    size += worklists[i].size();
  }
  return size;
}

template<typename VertexType, typename EdgeType>
int* Engine<VertexType, EdgeType>::get_worklist() {
  int size = get_worklist_size();
  int* worklist = (int*) malloc(sizeof(int)*size);
  int* prefix_sum = (int*) malloc(__cilkrts_get_total_workers() * sizeof(int));

  prefix_sum[0] = 0;
  for (int i = 1; i < __cilkrts_get_total_workers(); i++) {
    prefix_sum[i] = prefix_sum[i-1] + worklists[i-1].size();
  }

  cilk_for (int i = 0; i < __cilkrts_get_total_workers(); i++) {
    cilk_for (int j = 0; j < worklists[i].size(); j++) {
      worklist[prefix_sum[i] + j] = worklists[i][j];
    }
    worklists[i].clear();
  }

  free(prefix_sum);
  return worklist;
}

template<typename VertexType, typename EdgeType>
void Engine<VertexType, EdgeType>::record_set(int v, int depth) {
  if (depth != 0) return;
  int wid = __cilkrts_get_worker_number();
  if (__sync_bool_compare_and_swap(&recorded_set_bits[v], 0, 1)) {
    recorded_sets[wid].push_back(v);
  }
}

template<typename VertexType, typename EdgeType>
void Engine<VertexType, EdgeType>::asyncUpdate(int v, int depth) {
  int* neighbors = &sparse_rep.ColIds[sparse_rep.Starts[v]];
  int degree = sparse_rep.Starts[v+1] - sparse_rep.Starts[v];

  // execute the update.
  update_function(v);

  // decrement all bigger neighbors
  if (degree - vertex_data[v].partitionPoint < 256) {
    for (int i = vertex_data[v].partitionPoint; i < vertex_data[v].order_degree; i++) {
      int u = neighbors[i];
      //if (order[u] / coarsen_factor != order[v] / coarsen_factor) continue;
      if (vertex_data[u].counter > 1) {
        int ret = tfk_decrement(u, vertex_data);
        if (ret == 0) {
         cilk_spawn asyncUpdate(u, depth+1);
        }
      } else if (vertex_data[u].counter > 0) {
        vertex_data[u].counter = 0;
        cilk_spawn asyncUpdate(u, depth+1);
      }
    }
  } else {
    for (int i = vertex_data[v].partitionPoint; i < vertex_data[v].order_degree; i++) {
      int u = neighbors[i];
      //if (order[u] / coarsen_factor != order[v] / coarsen_factor) continue;
      if (vertex_data[u].counter > 1) {
        int ret = tfk_decrement(u, vertex_data);
        if (ret == 0) {
          cilk_spawn asyncUpdate(u, depth+1);
        }
      } else if (vertex_data[u].counter > 0) {
        vertex_data[u].counter = 0;
        cilk_spawn asyncUpdate(u, depth+1);
      }
    }
  }
}

template<typename VertexType, typename EdgeType>
void Engine<VertexType, EdgeType>::run(int* schedule, int* _iorder, void (*_update_function)(int)){

  if (scheduled_bits == NULL) {
    scheduled_bits = (int*) calloc(sparse_rep.numRows, sizeof(int));
    scheduled_round = 0;
  }

  recorded_set_bits = (int*) calloc(sparse_rep.numRows, sizeof(int));

  scheduled_round++;

  order = schedule;
  iorder = _iorder;
  update_function = _update_function;
  // Step 1: Initialize data structures.
  //counters = (int*) malloc(sizeof(int)*sparse_rep.numRows);
  //partitionPoint = (int*) malloc(sizeof(int) * sparse_rep.numRows);
  int* partitionPoint2 = (int*) malloc(sizeof(int) * sparse_rep.numRows);

  //order_degree = (int*) malloc(sizeof(int) * sparse_rep.numRows);

  vertex_data = (engine_vertex*) malloc(sizeof(engine_vertex) * sparse_rep.numRows);

  double start_init = tfk_get_time();
  // Step 2: Initialize the counters, given the order macro.
  cilk_for (unsigned int i = 0; i < sparse_rep.numRows; i++) {
    unsigned int* neighbors = (unsigned int*) &sparse_rep.ColIds[sparse_rep.Starts[i]];
    int degree = sparse_rep.Starts[i+1] - sparse_rep.Starts[i];
    int count = 0;
    int count2 = 0;
    int k = 0;
    unsigned int myOrder = order[i];

    for (int j = 0; j < degree; j++) {
      unsigned int neighbor = neighbors[j];
      unsigned int neighborOrder = order[neighbor];
      if (neighborOrder < myOrder) {
        int tmp = neighbors[k];
        neighbors[k++] = neighbors[j];
        neighbors[j] = tmp;
        if (myOrder / coarsen_factor == neighborOrder / coarsen_factor) {
          count++;
        }
        count2++;
      }
    }

    int block_counter = count2;
    for (int j = count2; j < degree; j++) {
      unsigned int neighbor = neighbors[j];
      unsigned int neighborOrder = order[neighbor];
      if (myOrder / coarsen_factor == neighborOrder / coarsen_factor) {
        unsigned int tmp = neighbors[j];
        neighbors[j] = neighbors[block_counter];
        neighbors[block_counter++] = tmp;
      }
    }

    vertex_data[i].counter = count;
    //counters[i] = count;
    //partitionPoint[i] = count2;
    vertex_data[i].partitionPoint = count2;
    vertex_data[i].partitionPoint2 = count;
    //order_degree[i] = block_counter;
    vertex_data[i].order_degree = block_counter;
  }
  double end_init = tfk_get_time();
  printf("total init time is %f\n", end_init - start_init);
  for (int j = 0; j < sparse_rep.numRows / coarsen_factor + 1; j++) {
    int limit = coarsen_factor*(j+1);
    if (limit > sparse_rep.numRows) limit = sparse_rep.numRows;
    cilk_for (int i = coarsen_factor * j; i < limit; i++) {
      int v = iorder[i];
      if (vertex_data[v].partitionPoint2 == 0) asyncUpdate(v, 0);
    }
  }
  printf("atomic operations %d\n", atomic_operations);
}

template<typename VertexType, typename EdgeType>
inline bool Engine<VertexType, EdgeType>::scheduled(int v){
  return scheduled_bits[v] == scheduled_round-1;
}

template<typename VertexType, typename EdgeType>
inline bool Engine<VertexType, EdgeType>::in_recorded_set(int v){
  return recorded_set_bits[v] == 1;
}

template<typename VertexType, typename EdgeType>
void Engine<VertexType, EdgeType>::schedule(int v){
  int wid = __cilkrts_get_worker_number();
  worklists[wid].push_back(v);
  scheduled_bits[v] = scheduled_round;
}

template<typename VertexType, typename EdgeType>
void Engine<VertexType, EdgeType>::run_worklist(int* schedule, int* worklist, int worklistsize, void (*_update_function)(int)){
  scheduled_round++;
  order = schedule;
  update_function = _update_function;

  // Step 1: Initialize data structures.
  // Assume that these were initialized earlier.
  //counters = (int*) calloc(sizeof(int),sparse_rep.numRows);
  //partitionPoint = (int*) calloc(sizeof(int) , sparse_rep.numRows);

  // Step 2: Initialize the counters, given the order macro.
  cilk_for (unsigned int w = 0; w < worklistsize; w++) {
  //cilk_for (unsigned int i = 0; i < worklistsize; i++) {
    int i = worklist[w];
    unsigned int* neighbors = (unsigned int*) &sparse_rep.ColIds[sparse_rep.Starts[i]];
    int degree = sparse_rep.Starts[i+1] - sparse_rep.Starts[i];
    int count = 0;
    int k = 0;
    unsigned int myOrder = order[i];
    for (int j = 0; j < degree; j++) {
      unsigned int neighbor = neighbors[j];
      unsigned int neighborOrder = order[neighbor];
      if (neighborOrder < myOrder) {
        int tmp = neighbors[k];
        neighbors[k++] = neighbors[j];
        neighbors[j] = tmp;
        if (scheduled(neighbor)) {
          count++;
        }
      }
    }
    vertex_data[i].counter = count;
    vertex_data[i].partitionPoint = count;
    vertex_data[i].order_degree = degree; // TODO(TFK): Implement coarsening for run_worklist. Right now there's no coarsening.
  }

  cilk_for (int w = 0; w < worklistsize; w++) {
    int i = worklist[w];
    if (vertex_data[i].partitionPoint == 0) asyncUpdate(i, 1);
  }
}

template<typename VertexType, typename EdgeType>
void Engine<VertexType, EdgeType>::loadGraph(std::string inputname) {
  // Parse the graph in .mtx or adjacency list representation.
  if (inputname.find(".mtx") != string::npos) {
    graph<int> inter = graphFromEdges<int>(edgesFromMtxFile<int>(inputname.c_str()), true);
    sparse_rep = sparseFromGraph<int>(inter);
  } else if (inputname.find(".edges") != string::npos) {
    graph<int> inter = graphFromEdges<int>(readEdgeArrayFromFile<int>((char*)inputname.c_str()), true);
    sparse_rep = sparseFromGraph<int>(inter);
  } else {
    graph<int> inter = graphFromEdges<int>(edgesFromGraph<int>(readGraphFromFile<int>((char*)inputname.c_str())), true);
    sparse_rep = sparseFromGraph<int>(inter);
  }
}
