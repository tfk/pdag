#include <assert.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <inttypes.h>
#include "../timeUtils.h"
#include "../graph_utils/graphUtils.h"
#include "../graph_utils/graphIO.h"
#include "../engine.cpp"

Engine<int,int>* engine;

double* edgeWeights;
double* edgeMessages;
double* vertexBeliefs;
double* outgoingMessageProduct;

void asyncBP(int v)  {
//    printf("processing vertex %d\n", v);
  if (v % 2 == 0) {
    engine->schedule(v);
  }
}

int main(int argc, char **argv)
{
  if (argc < 2) {
    printf("Usage: ./bp filename.{mtx,edges,adj}\n");
    return 0;
  }

  engine = new Engine<int,int>();
  double load_start = tfk_get_time();
  engine->loadGraph(std::string(argv[1]));
  double load_end = tfk_get_time();

  printf("Time to load the graph %f\n", load_end - load_start);
  double update_start = tfk_get_time();
  int* order = (int*) malloc(sizeof(int) * engine->sparse_rep.numRows);
  int* iorder = (int*) malloc(sizeof(int) * engine->sparse_rep.numRows);
  for (int i = 0; i < engine->sparse_rep.numRows; i++) {
    order[i] = i;
  }

  // perform shuffle
  for (int i = engine->sparse_rep.numRows-1; i >= 0; i--) {
    int j = ((unsigned int) rand())%(i+1);
    int tmp = order[i];
    order[i] = order[j];
    order[j] = tmp;
  }
  cilk_for (int i = 0; i < engine->sparse_rep.numRows; i++) {
    iorder[order[i]] = i;
  }

  engine->run(order, iorder, asyncBP);
  printf("done with first iteration\n");
  for (int i = 0; i < 20; i++) {
    int worklistsize = engine->get_worklist_size();
    int* worklist = engine->get_worklist();
    printf("worklist size is %d\n", worklistsize);
    engine->run_worklist(order, worklist, worklistsize, asyncBP);
//    engine->run(order, asyncBP);
    free(worklist);
  }
  double update_end = tfk_get_time();

  printf("Time to update the graph %f\n", update_end - update_start);
  return 0;
}

