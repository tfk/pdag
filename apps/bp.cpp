#include <assert.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <inttypes.h>
#include <queue>
#include <set>
#include "../timeUtils.h"
#include "../graph_utils/graphUtils.h"
#include "../graph_utils/graphIO.h"
#include "../engine.cpp"

Engine<int,int>* engine;

double* edgeWeights;
double* edgeMessages;
double* vertexBeliefs;
double* outgoingMessageProduct;

void asyncBP(int v)  {
  printf("the vertex %d\n",v);
  int degree = engine->sparse_rep.Starts[v+1] - engine->sparse_rep.Starts[v];
  int* neighbors = &engine->sparse_rep.ColIds[engine->sparse_rep.Starts[v]];
  double messageSum = 0;
  double messageProduct = 1;

  // compute product
  for (int i = 0; i < degree; i++) {
    messageSum += vertexBeliefs[v]*edgeWeights[engine->sparse_rep.Starts[v] + i];
    messageProduct *= outgoingMessageProduct[neighbors[i]] / edgeMessages[engine->sparse_rep.Starts[v]+i];
  }
  if (v == 0) {
    messageProduct *= vertexBeliefs[v];
    messageSum += vertexBeliefs[v]*1;
  }
  double previousProduct = outgoingMessageProduct[v];
  outgoingMessageProduct[v] = messageProduct;
  for (int i = 0; i < degree; i++) {
    double previousMessage = previousProduct / edgeMessages[engine->sparse_rep.Starts[v]+i];
    edgeMessages[engine->sparse_rep.Starts[v]+i] = outgoingMessageProduct[v] / previousMessage;
  }
}


int main(int argc, char **argv)
{
  if (argc < 2) {
    printf("Usage: ./bp filename.{mtx,edges,adj}\n");
    return 0;
  }

  engine = new Engine<int,int>();
  double load_start = tfk_get_time();
  engine->loadGraph(std::string(argv[1]));
  double load_end = tfk_get_time();

  uint64_t edgeCount = engine->sparse_rep.nonZeros;
  uint64_t vertexCount = engine->sparse_rep.numRows;
  printf("edgeCount is %llu\n", edgeCount);

  std::queue<int> queue;
  std::set<int> queueSet;
  std::queue<int> check;
  int sentinal = INT_MAX;
  int startV = 1;
  for (int i = 0; i < engine->sparse_rep.numRows; i++) {
    queue.push(i);
    queueSet.insert(i);
  }
  queue.push(sentinal);

  bool verifying = false;
  bool pushing = false;
  int max = engine->sparse_rep.numRows*2000;
  int startCheckingIter = engine->sparse_rep.numRows*1995;

  int verifyCount = 0;
  int progress = 0;
  for (int i = 0; i < max; i++) {
    if (i > (float)progress / 100 * max) {
      printf("progress %d\n",progress);
      progress++;
    }
    int v = queue.front();
    queue.pop();
    if (v == sentinal) {
      queue.push(sentinal);
      continue;
    }

    if (i > startCheckingIter) {
      if (v != startV && !verifying && pushing) {
        check.push(v);
      }
      if (v == startV && !verifying && !pushing) {
        pushing = true;
        check.push(v);
      } else if (v == startV && pushing) {
        printf("start verification\n");
        verifying = true;
        pushing = false;
        check.push(v);
        assert(check.front() == startV);
        check.pop();
      } else if (verifying && check.front() != v) {
        printf("Verification failed on vertex %d expected %d\n", v, check.front());
        return 0;
      } else if (verifying && v == startV) {
        printf("Verification succeeded! Count: %d\n", verifyCount);
        return 0;
      } else if (verifying) {
        if (check.front() != v) {
          printf("Error!");
          return 0;
        }
        verifyCount++;
        check.pop();
      }
    }
/*
      if (verifying) {
        if (check.front() != v) {
          printf("Verification failed on vertex %d\n", v);
          return 0;
          //while (!check.empty()) check.pop();
        }
        check.pop();
      }
      if (verifying && check.size() > 1 && v == startV) {
        //while (!check.empty()) check.pop();
        printf("Verification succeeded!\n");
        return 0;
      }
    }
*/

    int degree = engine->sparse_rep.Starts[v+1] - engine->sparse_rep.Starts[v];
    int* neighbors = &engine->sparse_rep.ColIds[engine->sparse_rep.Starts[v]];
    for (int j = 0; j < degree; j++) {
      int u = neighbors[j];
      if (queueSet.find(u) == queueSet.end()) {
        queueSet.insert(u);
        queue.push(u);
      }
    }
    queueSet.erase(v);
  }

  return 0;
}

