#include <assert.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <inttypes.h>
#include "../timeUtils.h"
#include "../graph_utils/graphUtils.h"
#include "../graph_utils/graphIO.h"
#include "../engine.cpp"

Engine<int,int>* engine;
int* vertexColors;

void validate_coloring(sparseRowMajor<int, int> & sparse_rep, int* vertexColors) {
  int max_color = -1;
  for (int v = 0; v < sparse_rep.numRows; v++) {
    int* neighbors = &sparse_rep.ColIds[sparse_rep.Starts[v]];
    int degree = sparse_rep.Starts[v+1] - sparse_rep.Starts[v];
    int color = vertexColors[v];
    if (color > max_color) max_color = color;
    for (int i = 0; i < degree; i++) {
      int neighbor = neighbors[i];
      if (vertexColors[neighbor] == color) {
        printf("Invalid coloring for vertex %d, %d color %d\n", v, neighbor, vertexColors[neighbor]);
        return;
      }
    }
  }
  printf("The number of colors is %d\n", max_color+1);
  printf("Valid coloring \n");
}

static __thread uint32_t* localNeighborColors = NULL;
static __thread int localNeighborColorsSize = 0;
inline void asyncColor(int v)  {
  //printf("coloring vertex %d\n", v);
  int* neighbors = &(engine->sparse_rep.ColIds[engine->sparse_rep.Starts[v]]);
  int degree = engine->sparse_rep.Starts[v+1] - engine->sparse_rep.Starts[v];
  if (localNeighborColors == NULL && degree < 128) {
    localNeighborColors = (uint32_t*) calloc(128, sizeof(uint32_t));
    localNeighborColorsSize = 128;
  }
  //char* localNeighborColors = (char*) calloc(sizeof(char),degree);
  if (localNeighborColorsSize <= degree+1) {
  //if (localNeighborColors == NULL) {
    localNeighborColors = (uint32_t*) calloc((degree+1), sizeof(uint32_t));
    localNeighborColorsSize = (degree+1);
  }
  //printf("partition point is %d\n", engine->vertex_data[v].partitionPoint);
  if (engine->vertex_data[v].partitionPoint < 256) {
/*
    for(int i = 0; i < engine->vertex_data[v].partitionPoint; i++) {
      _mm_prefetch((int*)vertexColors[neighbors[i]], 1);
    }
*/

    for(int i = 0; i < engine->vertex_data[v].partitionPoint; i++) {
      int u = neighbors[i];
      int color = vertexColors[u];
      if (color < degree) {
        localNeighborColors[color] = v+1;
      }
    }
  } else {
/*
  for(int i = 0; i < engine->vertex_data[v].partitionPoint; i++) {
    _mm_prefetch((int*)vertexColors[neighbors[i]], 1);
  }
*/
  for(int i = 0; i < engine->vertex_data[v].partitionPoint; i++) {
      int u = neighbors[i];
      int color = vertexColors[u];
      if (color < degree) {
        localNeighborColors[color] = v+1;
      }
    }
  }
  int color = -1;
  for (int i = 0; i < degree + 1; i++) {
    if (localNeighborColors[i] != v+1) {
      vertexColors[v] = i;
      break;
    }
  }
  //printf("coloring vertex %d with color %d\n", v, vertexColors[v]);
}

#define ORDER(p) p

int compare(unsigned int &a_i, unsigned int &b_i) {
  if( ORDER(a_i) != ORDER(b_i) )
    return ORDER(a_i) < ORDER(b_i) ? -1 : 1;
  else
    return a_i < b_i ? -1 : 1;
}


void sorted_order() {

}

int main(int argc, char **argv)
{
  if (argc < 2) {
    printf("Usage: ./color filename.{mtx,edges,adj}\n");
    return 0;
  }

  engine = new Engine<int,int>();
  double load_start = tfk_get_time();
  engine->loadGraph(std::string(argv[1]));
  double load_end = tfk_get_time();
  printf("Time to load the graph %f\n", load_end - load_start);
  double update_start = tfk_get_time();
  vertexColors = (int*) calloc(sizeof(int), engine->sparse_rep.numRows);

  int* worklist = (int*) calloc(sizeof(int), engine->sparse_rep.numRows);

  int* order = (int*) malloc(sizeof(int) * engine->sparse_rep.numRows);
  int* order2 = (int*) malloc(sizeof(int) * engine->sparse_rep.numRows);

  int* iorder = (int*) malloc(sizeof(int) * engine->sparse_rep.numRows);
  int* iorder2 = (int*) malloc(sizeof(int) * engine->sparse_rep.numRows);

  for (int i = 0; i < engine->sparse_rep.numRows; i++) {
    order[i] = i;
    order2[i] = engine->sparse_rep.numRows - i;
  }

  cilk_for (int i = 0; i < engine->sparse_rep.numRows; i++) {
    iorder[order[i]] = i;
    iorder2[order2[i]] = i;
  }
  engine->run(order, iorder, asyncColor);
  double update_end = tfk_get_time();
  //int distance = engine->measure_distance(order,iorder,order2,iorder2);
  //printf("The distance is %d\n", distance);
  validate_coloring(engine->sparse_rep, vertexColors);

  printf("Time to update the graph %f\n", update_end - update_start);
  return 0;
}

