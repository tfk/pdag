#ifndef TIME_UTILS_H
#define TIME_UTILS_H
// Simple timer for benchmarks
double tfk_get_time()
{
    struct timeval t;
    struct timezone tzp;
    gettimeofday(&t, &tzp);
    return t.tv_sec + t.tv_usec*1e-6;
}
#endif
