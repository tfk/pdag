#include <inttypes.h>
// cilkarts cilk++
#if defined(CILK)
#include <cilk.h>
#define parallel_main cilk_main
#define parallel_for cilk_for
#define parallel_for_1 _Pragma("cilk_grainsize = 1") cilk_for
#define parallel_for_256 _Pragma("cilk_grainsize = 256") cilk_for

// intel cilk+
#elif defined(CILKP)
#include <cilk/cilk.h>
#define parallel_for cilk_for
#define parallel_main main
//#define parallel_for_1 _Pragma("cilk grainsize = 1") cilk_for
//#define parallel_for_256 _Pragma("cilk grainsize = 256") cilk_for
#define parallel_for_1 cilk_for
#define parallel_for_256 cilk_for

// openmp
#elif defined(OPENMP)
#include <omp.h>
#define cilk_spawn
#define cilk_sync
#define parallel_main main
#define parallel_for _Pragma("omp parallel for") for
#define parallel_for_1 _Pragma("omp parallel for schedule (static,1)") for
#define parallel_for_256 _Pragma("omp parallel for schedule (static,256)") for

// c++
#else
#define cilk_spawn
#define cilk_sync
#define parallel_main main
#define parallel_for for
#define parallel_for_1 for
#define parallel_for_256 for
#define cilk_for for

#endif

#include <limits.h>

#if defined(LONG)
//typedef long intT;
//typedef unsigned long uintT;
typedef int64_t intT;
typedef uint64_t uintT;
#define INT_T_MAX LONG_LONG_MAX
#else
//typedef int intT;
//typedef unsigned int uintT;
typedef int64_t intT;
typedef uint64_t uintT;
#define INT_T_MAX INT64_MAX
#endif
